-- Tile map loader
-- Responsible for parsing .map files and generating a resulting data structure. 

module MapLoader.Loader
( loadMap
, emptyMap
, getTileLayers
, getObjectLayers
, getCell
, Map(..)
) where

import           System.IO
--import qualified Data.Map as M
 
-- I probably need to define some custom types
-- types needed:
--                 - Map            -> Represents the map itself
--                  - TileLayer     -> Represents a layer of the map
--                      - MapCell   -> Represents a cell of the map
--                  - ObjectLayer   -> Represents an object layer of the map
--                      - MapObject -> Represents a map object (simple geometry)
-- TiledMap is a composite type wich is composed of the other sub types listed above
-- I'm not sure what kind of data structure I should use to represent the map though.
-- It should be efficient and easily searchable...
-- I need to figure out how I want to parse the data.

-- MapObject: 
-- Rect: ID, Name, Type, Visible, X, Y, Width, Height, Rotation
-- Ellipse: ID, 
-- Polygon: 
-- Polyline:

-- The following record definitions are going to cause issues.
-- They contain duplicate fields and since Haskell has no namespacing
-- they get treated as the same name, which of course causes naming 
-- conflicts...
-- There are a couple of ugly hacks to get around this,
-- 1. prefix each field with the it's record name
-- 2. Put each record into it's own namespace.
-- Both of those are considered ugly and hacky.
-- There exists a Record library however which apparently
-- solves this issue very elegantly. I need to look into using that.
data Info = Info { } deriving (Show)

-- Ugly hack, ugh. I might need to use the Record library when I can.
-- Or possibly just use maps?

data Shape = Rect { rInfo :: Info }
           | Ellipse { eInfo :: Info }
           | Polygon { pInfo :: Info }
           | Polyline { plInfo :: Info }
           deriving (Show)

data MapObject = Maybe Shape deriving (Show)

data ObjectLayer = ObjectLayer { objects :: [MapObject] } deriving (Show) 

data MapCell = MapCell { cX :: Int
                       , cY :: Int
                       , symbol :: Char
                       } deriving (Show)

data TileLayer = TileLayer { tiles :: [MapCell] } deriving (Show)

data Map = Map { path :: String
               , tileSize :: Int
               , mWidth :: Int
               , mHeight :: Int
               , objectLayers :: [ObjectLayer]
               , tileLayers :: [TileLayer]
               } deriving (Show)

-- Need to parse a .map file
loadMap :: String -> Maybe Map
loadMap path = Nothing

emptyMap :: Map
emptyMap = Map { path = ""
               , tileSize = 0
               , mWidth = 0
               , mHeight = 0
               , objectLayers = []
               , tileLayers = []
               }

getTileLayers :: Map -> Maybe [TileLayer]
getTileLayers m = Nothing

getObjectLayers :: Map -> Maybe [ObjectLayer]
getObjectLayers m = Nothing

getCell :: Int -> Int -> Maybe MapCell
getCell x y = Nothing
