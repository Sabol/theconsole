module Types where

import qualified MapLoader.Loader as L

-- I am going to need a way to represent the shape of the
-- entities... I should possibly abstract a square shape out into
-- a Square data type. I'm not even sure how to go about doing that
-- with OpenGL though... Do I need to just define some vertices, two
-- triangles?

-- Alias Char as Symbol
type Symbol = Char

data Position = Position Int Int 

-- how do I represent the game modes?
data GameMode = Pause | Play | Console | Menu deriving (Show)

-- This needs to be rendered somehow, since I am just going to use fonts
-- I can actually just have a char to represent entities.
data Entity = Entity { x :: Int
                     , y :: Int
                     , prevX :: Int
                     , prevY :: Int
                     , w :: Int
                     , h :: Int
                     , tag :: String
                     , symbol :: Symbol 
                     } deriving (Show, Eq)

-- The GameState data type represents the current state of 
-- the game world.
-- What should the game state data type keep track of?
-- A list of entities which may be rendered
-- A Map type instance
data GameState = GameState { entities :: [Entity]
                           , grid :: Grid
                           , gameMap :: L.Map
                           , mode :: GameMode 
                           } deriving (Show)


-- The grid to render to and track the position of everything.
-- This is also what will be rendered to the screen as well.
-- The grid is contains Symbols which is an alias for Char
-- The grid will then be update with the map symbols and the 
-- entity symbols on each game loop pass. 
type Grid = [[Symbol]]

