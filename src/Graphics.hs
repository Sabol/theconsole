module Graphics
( initDisplay 
, closeDisplay
--, setFont
, fontColor
, renderText
, renderBuffer
) where

-- Maybe put shader stuff in here too?

import           SDL (($=))
import qualified SDL
import           SDL.TTF as TTF
import           SDL.TTF.FFI (TTFFont) 
import qualified SDL.Raw.Types as SDLT
import qualified Graphics.Rendering.OpenGL as GL

import Types


-- Start yer engines lads
initDisplay :: Int -> Int -> String -> IO () 
initDisplay = do
  -- Initilaize SDL
  SDL.initialize [SDL.InitVideo]
  -- Not too sure what this is about, need to read deeper into it.
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  do renderQuality <- SDL.get SDL.HintRenderScaleQuality
     when (renderQuality /= SDL.ScaleLinear) $
       putStrLn "Warning: Linear texture filtering not enabled!"

  TTF.init

  -- Create a window
  window <-
    SDL.createWindow
      "The Console"
      SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight,
                         -- Create an OpenGl window 
                         SDL.windowOpenGL = Just SDL.defaultOpenGL}


--setFont :: String -> TTFFont

fontColor :: Int -> Int -> Int -> Int -> SDLT.Color
fontColor = SDLT.Color

renderText :: String -> TTFFont -> SDL.Surface -> Int -> Int IO ()
renderText string font surface x y = do
  text <- TTF.renderTextSolid font string fontColor
  SDL.surfaceBlit text Nothing surface Nothing

-- Render a Console buffer
-- A Console buffer is just [Char]
-- treated like a 2D array
-- formula for access => rowIndex * numCols + colIndex
-- i.e. px = 10 py = 4 numCols = 10 => 4 * 10 + 10 = 50
--      50 would be the index of the entity in the Console buffer,
--      while [10][4] would be the index position of the entity in the
--      game grid
renderBuffer :: [Char] -> IO ()
