module Game
( --gameLoop 
  moveEntity
) where

import Types
import Helpers
import Data.List

-- This is where I can tie all the calculations together that would make up the
-- game, I can also export the main game loop.

-- Warning: impure function
--gameLoop :: GameState -> GameState IO ()

-- First derive a new entity from the previous entity and the position
-- then derive a new game state with an update entities list
-- then derive a new game state with an updated char grid
-- finally return the new game state.
moveEntity :: Entity -> GameState -> Position -> GameState 
moveEntity ent state (Position px py) = 
  let newEnt     = Entity { x = px
                          , y = px
                          , prevX = x ent
                          , prevY = y ent
                          , w = w ent
                          , h = h ent
                          , tag = tag ent
                          , symbol = symbol ent
                          }
      newEntities = newEnt : (delete ent $ entities state)
      newGrid     = updateGrid (grid state) (symbol newEnt) px py  
  in GameState { entities = newEntities 
               , grid     = newGrid
               , gameMap  = gameMap state
               , mode     = mode state
               }

      

