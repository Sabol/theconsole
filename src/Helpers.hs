module Helpers 
( accessGrid
, replaceElem
, updateGrid
) where

-- Given a 2d list and an i and j index return the item at the specified idices
accessGrid :: (Eq a) => [[a]] -> Int -> Int -> a
accessGrid m i = (!!) ((!!) m i) 

-- look through list, tracking position
-- tack on each elem to new list until 
-- index is reached then tack on new elem
-- concat rest of list to new list.
replaceElem :: [a] -> a -> Int -> [a]
replaceElem xs x i 
  | i == 0    = x : (tail xs)
  | otherwise = head xs : (replaceElem (tail xs) x (i-1))   

-- This is going to be expensive to do..
-- Derive new grid from old one
updateGrid :: [[a]] -> a -> Int -> Int -> [[a]] 
updateGrid xs elem i j = 
  let row    = xs !! i
      -- Derive a new row
      newRow = replaceElem row elem j
  -- Derive a new grid now
  in replaceElem xs newRow i
