{-# LANGUAGE CPP #-}
{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Control.Monad -- I still don't understand what a Monad actually is...
import           Foreign.C.Types
import           Linear
import qualified Data.ByteString as BS
import qualified Data.Vector.Storable as V
import           Data.Word (Word8)
import           System.Exit (exitFailure)
import           System.IO

import           SDL (($=))
import qualified SDL
import           SDL.TTF as TTF
import           SDL.TTF.FFI (TTFFont)
import qualified SDL.Raw.Types as SDLT  
import qualified Graphics.Rendering.OpenGL as GL

import           Types
import qualified MapLoader.Loader as L
import           Helpers
import           Game

#if !MIN_VERSION_base(4,8,0)
import Control.Applicative
#endif

screenWidth, screenHeight :: CInt
(screenWidth, screenHeight) = (640, 480)

fontColor :: SDLT.Color
fontColor = SDLT.Color 255 255 255 255

-- Should do a check to see if this exists.
fontPath :: String
fontPath = "./assets/font.ttf"

-- Define some simple rendering functions here
-- They will get moved to Graphics.hs eventually.
-- This is a side effect producing function.
-- It performs IO operations.
drawText :: String -> TTFFont -> SDL.Surface -> Int -> Int -> IO () 
drawText string font surface x y = do 
    text <- TTF.renderTextSolid font string fontColor  
    -- Blit the font to the window surface?
    SDL.surfaceBlit text Nothing surface Nothing 

-- Let the side effects begin!
main :: IO ()
main = do
  
  -- Initilaize SDL
  SDL.initialize [SDL.InitVideo]
  -- Not too sure what this is about, need to read deeper into it.
  SDL.HintRenderScaleQuality $= SDL.ScaleLinear
  do renderQuality <- SDL.get SDL.HintRenderScaleQuality
     when (renderQuality /= SDL.ScaleLinear) $
       putStrLn "Warning: Linear texture filtering not enabled!"

  TTF.init

  -- Create a window
  window <-
    SDL.createWindow
      "The Console"
      SDL.defaultWindow {SDL.windowInitialSize = V2 screenWidth screenHeight,
                         -- Create an OpenGl window 
                         SDL.windowOpenGL = Just SDL.defaultOpenGL}
    
  -- Get the window surface
  surface <- SDL.getWindowSurface window

  -- Show the window i.e make it desplay.
  SDL.showWindow window

  -- Create a renderer
  renderer <- SDL.createRenderer window (-1) SDL.defaultRenderer

  -- Create an OpenGl context using our window
  -- we don't care to keep what it returns though.
  -- This is probably going to be used for shaders.
  _ <- SDL.glCreateContext window

  -- open the font
  -- Need to figure out what that Int param is for...
  font <- TTF.openFont fontPath 18  
  print font
  --TTF.closeFont font

  -- Load up the map
  let gMap = L.emptyMap

  -- Create the initial game state
  let gameState = GameState { entities = []
                            , grid = [[]] 
                            , gameMap = gMap 
                            , mode = Menu
                            }

  -- This is the game loop
  let loop :: SDL.Renderer -> IO () 
      loop renderer = do
        -- General steps
        -- 1. update the GameState by deriving a new one from input, etc.
        --  1a. update the Grid with entitiy positions
        --      - May be a way to optimize all of this.. Will think on that later.
        --  1b. This can mean removing or adding tiles to the map as well
        -- 2. Render the Grid using SDL-ttf.
        --  2a. Apply any shaders as well.
        --
        -- The console will also operate off of the Grid of Chars but with different logic
        -- to that of the game.
        --
        -- get any events such as keyboard events
        events <- SDL.pollEvents
        -- if we encounter the SDL.QuitEvent
        -- any takes a predicate and a list and returns True if any elem causes
        -- the predicate to evaluate to True, and returns False otherwise.
        let quit = elem SDL.QuitEvent $ map SDL.eventPayload events
        
        let kbev = map SDL.eventPayload events

        -- I think this gives me only the events that are keyboard events.
        let isKbEvent event =
                case SDL.eventPayload event of
                    SDL.KeyboardEvent keyboardEvent -> True
                    _ -> False
            kbevs = foldl (\acc e ->
                            if isKbEvent e then
                                e : acc
                            else
                                acc
                          )
                    [] events

        SDL.rendererDrawColor renderer $= V4 0 0 0 255
        text <- TTF.renderTextSolid font "Hey, we have some text!" (SDLT.Color 255 255 255 255) 
        SDL.surfaceBlit text Nothing surface Nothing
        SDL.clear renderer
        SDL.freeSurface text
        SDL.present renderer
        SDL.updateWindowSurface window

        -- The application breaks when the window is moved for some reason...
        -- The text also flickers a lot, I need to fix that too.

        -- Clear the screen I suppose?
        --GL.clear [GL.ColorBuffer]
        -- Draw some stuff
        -- draw prog attrib
        --SDL.glSwapWindow window

        -- Unless quit is logically True, call loop again.
        unless quit (loop renderer)

  -- Start the game loop on it's way.
  loop renderer

  TTF.closeFont font
  TTF.quit
  -- Get rid of the window
  SDL.destroyWindow window
  -- Quit SDL
  SDL.quit
